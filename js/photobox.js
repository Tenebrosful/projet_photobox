import photoloader from "./photoloader.js";
import gallery from "./gallery.js";

$(document).ready(() => {
    photoloader.init("https://webetu.iutnc.univ-lorraine.fr").then(() => {
        gallery.init(0, "/www/canals5/photobox/photos/?offset=0&size=10");
        $("#load_gallery").click(gallery.load);
        $("#previous").click(gallery.prev);
        $("#next").click(gallery.next);
    });
});