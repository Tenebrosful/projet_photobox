import photoloader from "./photoloader.js";

function open() {
    let img = $(this);

    getInfos(img[0].dataset.uri).then((result) => {
        let info = result.data.photo;
        getCommentaires(result.data.links.comments.href).then((result) => {
            let lightbox = create(img[0].alt, img[0].dataset.img, info.descr, info.format, info.size, info.height, info.width);
            lightbox.find("#lightbox").append(createCommentaires(result));
            $("#gallery").prepend(lightbox);
        });

    });

}

/**
 *
 * @param {string} titre
 * @param {string} src
 * @param desc
 * @param format
 * @param taille
 * @param hauteur
 * @param largeur
 * @return {jQuery|HTMLElement}
 */
function create(titre, src, desc, format, taille, hauteur, largeur) {
    let lighbox = $(`<div class='lightbox_container' id='lightbox_container'>
                <div id='lightbox'>
                    <div id='lightbox-head'>
                        <div id='lightbox_title'>${titre}</div>
                    </div>
                    <div id='lightbox-img'>
                        <img id='lightbox_full_img' src='${src}'>
                    </div>
                    <div id='lightbox-info'>
                        <div class="lightbox-info">
                            <div class="lightbox-nomCarac">Descrition</div>
                            <div class="lightbox-carac">${desc}</div>
                        </div>
                        <div class="lightbox-info">
                            <div class="lightbox-nomCarac">Format</div>
                            <div class="lightbox-carac">${format}</div>
                        </div>
                        <div class="lightbox-info">
                            <div class="lightbox-nomCarac">Taille</div>
                            <div class="lightbox-carac">${taille}</div>
                        </div>
                        <div class="lightbox-info">
                            <div class="lightbox-nomCarac">Dimensions</div>
                            <div class="lightbox-carac">${largeur}x${hauteur}</div>
                        </div>
                    </div>
                </div>
            </div>`);

    lighbox.find("#lightbox").find("#lightbox-head").append($("<button id='lightbox_close'>❌</button>").click(deleteLightbox));

    return lighbox;
}

function getInfos(pathInfo) {
    return photoloader.load(pathInfo);
}

function getCommentaires(pathCommentaires) {
    return photoloader.load(pathCommentaires);
}

function createCommentaires(result) {
    console.log(result);
    let coms = result.data.comments;

    let res = $("<div id='lightbox-commentaires'>");

    coms.forEach(e => {
        let com = $("<div class='lightbox-commentaire'>");
        com.append($(`<div class='lightbox-commentaire-head'>${e.titre} par ${e.pseudo} le ${e.date}</div>`));
        com.append($(`<div class='lightbox-commentaire-content'>${e.content}</div>`));
        res.append(com);
    });

    return res;
}

function deleteLightbox() {
    $("#lightbox_container").remove();
}

export default {
    open: open,
}