let url_source;

let config =
    {
        responseType: "json",
        withCredentials: true
    };

/**
 *
 * @param {string} server_url
 * @return {Promise}
 */
async function init(server_url = "") {
    return axios.get(server_url, config).then(function () {
        url_source = server_url;
        console.log(url_source);
    }).catch(error => {
        alert(`Une erreur est survenue lors de l'initialisation, impossible de contacter ${server_url}`);
    });
}

/**
 *
 * @param {string} path
 */
function load(path) {

    let size;
    let tmpConfig = {...config};

    let url = new URL(path, url_source);

    if ((size = url.searchParams.get("size")) !== null)
        tmpConfig.size = size;

    return axios.get(url, tmpConfig).catch(error => {
        console.log("Une erreur est survenue lors du chargement des images", error);
    })
}

export default {
    init: init,
    load: load,
}