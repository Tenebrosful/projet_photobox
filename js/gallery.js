import photoloader from "./photoloader.js";
import lightbox from "./lightbox.js";

let id, path, links;

/**
 *
 * @param {int} idGallery
 * @param {string} pathGallery
 */
function init(idGallery, pathGallery) {
    id = idGallery;
    path = pathGallery;
}

function load() {
    photoloader.load(path).then(traitement);
}

$.fn.exists = function () {
    return this.length !== 0;
};

function traitement(result) {

    let data = result.data.photos;

    console.log(data);

    links = result.data.links;

    console.log(path, links);

    let divGallery = $('#photobox-gallery');

    if (!divGallery.exists()) {
        divGallery = $("<div class='gallery-container' id='photobox-gallery'>");
        $("#gallery").append(divGallery);
    } else {
        divGallery.empty();
    }

    data.forEach((photo) => {
        let dataimg = photo["photo"]["original"].href;
        let datauri = photo["links"].self.href;
        let src = photo["photo"]["thumbnail"].href;
        let name = photo["photo"]["titre"];

        let img = $("<img alt='" + name + "' data-img='" + dataimg + "' data-uri='" + datauri + "' src='" + src + "'>").click(lightbox.open);
        let nom = $("<div>" + name + "</div>");
        let vignette = $("<div class='vignette'>");

        vignette.append(img);
        vignette.append(nom);

        divGallery.append(vignette);
    });
}

function prev() {
    if (links["first"].href === path)
        path = links["last"].href;
    else
        path = links["prev"].href;

    load();
}

function next() {
    if (links["next"].href === path)
        path = links["first"].href;
    else
        path = links["next"].href;

    load();
}

export default {
    init: init,
    load: load,
    prev: prev,
    next: next,
}